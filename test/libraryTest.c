#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    int** matrix_entry = (int *) malloc(1*sizeof(int));
    matrix_entry[0] = (int*) malloc(1*sizeof(int));
    matrix_entry[0][0] = -8;
    
    // When
    int amount = valuesNumber(-4, matrix_entry, 1, 1);
    // Then
    assertEquals_int(0, amount);
}


void testExercise_B() {
	// Given
	    int** matrix_entry = (int *) malloc(1*sizeof(int));
	    matrix_entry[0] = (int*) malloc(1*sizeof(int));
	    matrix_entry[0][0] = -1;
	    matrix_entry[0][1] = -1;

	    // When
	    int amount = valuesNumber(-1, matrix_entry, 1, 2);
	    // Then
	    assertEquals_int(2, amount);
	}

void testExercise_C() {
	// Given
	    int** matrix_entry = (int *) malloc(1*sizeof(int));
	    matrix_entry[0] = (int*) malloc(1*sizeof(int));
	    matrix_entry[0][0] = 22;
	    matrix_entry[0][1] = 22;
	    matrix_entry[1] = (int *)malloc(1*sizeof(int));
	    matrix_entry[1][0] = 22;

	    // When
	    int amount = valuesNumber(22, matrix_entry, 3, 1);
	    // Then
	    assertEquals_int(3, amount);
	}

void testExercise_D() {
    // Given
    int* matrizDeEnteros[3];
    int* matrizDeEnteros[0] = (int*) malloc(2 * sizeof(int));
    int* matrizDeEnteros[3];
    int* matrizDeEnteros[3];
    int* matrizDeEnteros[3];
    // When
    
    // Then
    fail("Not yet implemented");
}

void testExercise_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
