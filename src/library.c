#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int valuesNumber(int foundValue, int** value, int valueRows, int valueColumns) {
    int valueamount = 0;
    for(int row = 0; row <valueRows; row++) {
        for(int column =0; column <valueColumns; column++) {
            if(value[row][column] == foundValue) {
            	valueamount++;
            }
        }
    }
    return valueamount;
}
