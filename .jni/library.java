/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "valuesNumber(int,int**,int,int):int")
    public int valuesNumber(int foundValue, int[][] value, int valueRows, int valueColumns){
        return valuesNumber_(foundValue, value, valueRows, valueColumns);
    }
    private native int valuesNumber_(int foundValue, int[][] value, int valueRows, int valueColumns);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
